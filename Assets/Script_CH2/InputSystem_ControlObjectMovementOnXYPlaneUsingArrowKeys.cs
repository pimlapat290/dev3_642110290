using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputSystem_ControlObjectMovementOnXYPlaneUsingArrowKeys : MonoBehaviour
{
    public float m_MovemenStep;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Keyboard keyboard = Keyboard.current;
        // Getkey
        if (keyboard[Key.LeftArrow].isPressed)
        {
            this.transform.Translate(-m_MovemenStep,0,0);
        }
        else if (keyboard[Key.RightArrow].isPressed)
        {
            this.transform.Translate(m_MovemenStep,0,0);
        }
        else if (keyboard[Key.UpArrow].isPressed)
        {
            this.transform.Translate(0,m_MovemenStep,0);
        }
        else if (keyboard[Key.DownArrow].isPressed)
        {
            this.transform.Translate(0,-m_MovemenStep,0);
        }
    }
}
