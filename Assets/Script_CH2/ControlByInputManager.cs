using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlByInputManager : MonoBehaviour
{
    public float m_MovementScale = 0.5f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float hMovement = Input.GetAxis("Horizontal") * m_MovementScale;
        float VMovement = Input.GetAxis("Vertical") * m_MovementScale;
        
        this .transform.Translate(new Vector3(hMovement,VMovement,0));
        if (Input.GetButtonDown("Fire1"))
        {
            ShootABulletInForwardDirection(Vector3.zero);
        }
        if (Input.GetButtonDown("Fire2"))
        {
            ShootABulletInForwardDirection(new Vector3(0,1,0),PrimitiveType.Capsule,1.5f);
        }
    }

    private void ShootABulletInForwardDirection(Vector3 forceModifier, PrimitiveType type = PrimitiveType.Sphere,
        float forceMagnitude = 1)
    {
        //Create a new primitive at runtime
        var newGameObject = GameObject.CreatePrimitive(type);
        
        //Create a new primitive at runtime
        newGameObject.transform.position = transform.position + transform.forward * 1.5f;
        
        //Add rigidbody to the newly created gameobject
        var rigidbody = newGameObject.AddComponent<Rigidbody>();
        rigidbody.mass = 0.15f;
        
        //Calculate the shooting direction
        Vector3 ShootingDirection = (forceModifier + transform.forward) * forceMagnitude;
        rigidbody.AddForce(ShootingDirection,ForceMode.Impulse);
        
        //Destroy the new gameobject in 3 sec
        Destroy(newGameObject,3);
    }
}
