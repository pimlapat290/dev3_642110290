using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlObjectMovementOnXYPlaneUsingTFGH : MonoBehaviour
{
    public float m_MovemenStep;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKey(KeyCode.F))
        {
            this.transform.Translate(-m_MovemenStep,0,0);
        }
        else if (Input.GetKey(KeyCode.H))
        {
            this.transform.Translate(m_MovemenStep,0,0);
        }
        else if (Input.GetKey(KeyCode.T))
        {
            this.transform.Translate(0,m_MovemenStep,0);
        }
        else if (Input.GetKey(KeyCode.G))
        {
            this.transform.Translate(0,-m_MovemenStep,0);
        }
        
        
        if (Input.GetKey(KeyCode.F))
        {
            this.transform.Translate(-m_MovemenStep*(-1),0,0);
        }
        else if (Input.GetKey(KeyCode.H))
        {
            this.transform.Translate(m_MovemenStep*(-1),0,0);
        }
        else if (Input.GetKey(KeyCode.T))
        {
            this.transform.Translate(0,m_MovemenStep*(-1),0);
        }
        else if (Input.GetKey(KeyCode.G))
        {
            this.transform.Translate(0,-m_MovemenStep*(-1),0);
        }
    }
}
